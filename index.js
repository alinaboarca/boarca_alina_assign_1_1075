
const FIRST_NAME = "BOARCA";
const LAST_NAME = "ALINA";
const GRUPA = "1075";


function numberParser(value) {
    if(value===-Infinity || value===Infinity || value>Number.MAX_SAFE_INTEGER ||value<Number.MIN_SAFE_INTEGER || isNaN(value))
		return NaN
	else return parseInt(value) 
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}


